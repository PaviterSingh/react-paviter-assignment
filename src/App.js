import React from "react";
import NavBar from "./components/Navbar";
import SearchBar from "./components/SearchBar";
import Cart from "./components/Cart";
// Data for the site
const data = {
    //Intialy cart in empty
  Cart: [],
  Mobile: [
    {
      id: 11,
      image: "SamsungM12.png",
      price: 11499,
      name: "Samsung Galaxy M12",
      description:
        "16.55cm (6.5”) HD+ Infinity-V Display with 90Hz Screen refresh rate ,True 48MP Quad Camera with Isocell GM2 sensor, 6,000mAh Long-lasting Battery with 15W Adaptive Fast Charging",
    },
    {
      id: 12,
      image: "OnePlusNordCE2.jpg",
      price: 24999,
      name: "OnePlus Nord CE 2 5G ",
      description:
        "Model Name	OnePlus Nord CE 2 5G, Wireless Carries,Unlocked for All Carriers, Brand	OnePlus,Memory Storage Capacity	128 GB,OS	OxygenOS",
    },
    {
      id: 13,
      image: "RedmiNote11.jpg",
      price: 13499,
      name: "Redmi Note 11",
      description:
        "Wireless Carrier-Unlocked for All Carriers, Brand-Redmi, Form factor	Bar,Memory Storage - 64GB",
    },
    {
      id: 14,
      image: "Xiaoi11Lite.jpg",
      price: 26999,
      name: "Xiaomi 11 Lite NE 5G",
      description:
        "Xiaomi 11 Lite NE 5G (Jazz Blue 6GB RAM 128 GB Storage) | Slimmest (6.81mm) & Lightest (158g) 5G Smartphone | 10-bit AMOLED with Dolby Vision | Additional Off up to 5000 on Exchange",
    },
    {
      id: 15,
      image: "AppleiPhone12.jpg",
      price: 60999,
      name: "Apple iPhone 12",
      description:
        "6.1-inch (15.5 cm diagonal) Super Retina XDR display,Ceramic Shield, tougher than any smartphone glass, A14 Bionic chip, the fastest chip ever in a smartphone,Advanced dual-camera system with 12MP Ultra Wide and Wide cameras; Night mode, Deep Fusion, Smart HDR 3,4K Dolby Vision HDR recording,12MP TrueDepth front camera with Night mode, 4K Dolby Vision HDR recording",
    },
  ],

  Earphones: [
    {
      id: 21,
      image: "boatBassheads100.jpg",
      price: 399,
      name: "boAt Bassheads 100",
      description:
        "The stylish BassHeads 100 superior coated wired earphones are a definite fashion statement - wear your attitude with its wide variety of collection",
    },
    {
      id: 22,
      image: "Jblc1000.jpg",
      price: 649.0,
      name: "JBL C100SI",
      description:
        "xtra Deep Bass. Troubleshooting steps : Kindly ensure 3.5mm port on Host device is clean and dust free and 3.5mm jack of the earphone is adequately inserted inside the input device port",
    },
    {
      id: 23,
      image: "BoultAudioAriBass.jpg",
      price: 1299,
      name: "Boult Audio AirBass TrueBuds TWS Earbuds ",
      description:
        "One Touch Control & Voice Assistant: With one multifunction button, you can play/pause, previous/next track and answer/hang-up calls.Voice assistant function lets you access siri/Google",
    },
    {
      id: 24,
      image: "OneplusBulletsZbass.jpg",
      price: 1999,
      name: "Oneplus Bullets Wireless Z Bass",
      description:
        "The Bass Edition comes equipped with Bluetooth v5.0 and is fully compatible with all smartphones.",
    },
    {
      id: 25,
      image: "61gcfvgIpgL._SL1500_.jpg",
      price: 17999,
      name: "LG Tone Free FP9W - Active Noise Cancelling",
      description:
        "【ACTIVE NOISE CANCELLATION】More Immersion than ever, less noise than before. How LG specialized ANC works; built-in high performance upper microphone detects external noise, inner microphone cancels out unwanted noise*",
    },
  ],
  Laptop: [
    {
      id: 31,
      image: "81Ln6w1hqtL._SL1500_.jpg",
      price: 42999,
      name: "HP 15s 11th Gen Intel Core i3- 8GB RAM/512GB SSD 15.6-inch(39.6 cm) Laptop",
      description:
        "Memory & Storage: 8 GB DDR4-3200 MHz RAM (1 x 8 GB), Upto 16 GB DDR4-3200 MHz RAM (2 x 8 GB) | Storage: 512GB PCIe NVMe M.2 SSD",
    },
    {
      id: 32,
      image: "51IsQHLroNL._SL1080_.jpg",
      price: 40099,
      name: "Dell New Vostro 3405 Laptop AMD Ryzen 3-3250U, 8GB DDR4",
      description:
        "Ports: USB 3.2 Gen 1 (x2), USB2.0 (x1), HDMI 1.4, SD Media Card Reader (SD, SDHC, SDXC), RJ45 - 10/100Mbps BT & WiFi- 802.11ac 1x1 WiFi and Bluetooth",
    },
    {
      id: 33,
      image: "61WNxdAeAoL._SL1000_.jpg",
      price: 82799,
      name: 'Lenovo IdeaPad Slim 5 11th Gen Intel Core i5 15.6"(39.62cm) FHD IPS Thin',
      description:
        'Lenovo IdeaPad Slim 5 11th Gen Intel Core i5 15.6"(39.62cm) FHD IPS Thin & Light Laptop (16GB/512GB SSD/Windows 11/MS Office 2021/Backlit Keyboard/Fingerprint Reader/Graphite Grey/1.66Kg), 82FG01H9IN',
    },
  ],
};
class App extends React.Component {
  state = { Category: [] }; //contain data i.e array of moblies
  cat = ""; 
  //2 way callbact function ,when a catergory
  // parameter e is string Mobile or earphones 
  onCategorySelect = (e) => {
      //cat variable is used to check that CArt is selected or not
    this.cat = e;
    this.setState({ Category: data[e] });
  };
  //2 way callback function to called when Add to card button is clicked
  // parameter single element of an array
  onAddCart(e) {
      // this logic to find unique element in cart so there are no duplicate items
    const test = data.Cart.find((element) => element.id === e.id);
    // if test is undefined then item already exist in cart
    if (test !== undefined) {
      alert("item already present in cart");
    } else {
        // if does not exist in cart then push to cart array
      data.Cart.push(e);
    }
  }
  // this is 2 way callback function to delete an item form the cart
  // parameter items id
  onDeleteItem = (id) => {
      // if id is string all then delete all the items
    if (id === "all") {
      data.Cart = [];
    }
    //ndata contain itesm which does not have id = id
    const ndata = data.Cart.filter((element) => element.id !== id);
    data.Cart = ndata;
    // not the array/data in category in cart's data with deleted element
    this.setState({ Category: ndata });
  };
  // to Check which component to render Cart or SEarchbar
  check() {
    if (this.cat === "Cart") {
      return <Cart onDeleteItem={this.onDeleteItem} items={data.Cart} />;
    }
    // intial state of app when no category is selected 
    else if (this.state.Category.length === 0) {
      return (
        <div>
          <h1
            className="ui yellow header"
            style={{
              position: "absolute",
              top: "40vh",
              left: "25vw",
              justifyContent: "center",
              width: "50vw",
            }}
          >
            Welcome Viewer to this eCommerce website, You can choose item of
            your choice from the above Menu i.e Earphones, Laptop etc
            <br/>
            you increase the quantity of items using cart and delete item too
          </h1>
        </div>
      );
    }

    return (
      <SearchBar Category={this.state.Category} onAddCart={this.onAddCart} />
    );
  }
  render() {
    return (
      <div style={{ backgroundColor: "lightblue" }}>
        <NavBar onCategorySelect={this.onCategorySelect} />
        {this.check()}
      </div>
    );
  }
}

export default App;
