import React from "react";

class SearchList extends React.Component {
  onAddCart = (item) => {
    this.props.onAddCart(item);
  };
  render() {
    if (!this.props.category) {
      return <div>loading</div>;
    }// return a array of items
    const img = this.props.category.map((e) => {
      return (
        <div key={e.id} className="ui card grid-item">
          <div className="image">
            <img src={require("../images/" + e.image)} alt={e.name} />
          </div>
          <div className="content">
            <p className="header">{e.name}</p>
            <div className="description">
              {e.description}
              <br />
              <h4>Price: {e.price} rupees</h4>
            </div>
          </div>
          <div className="extra content">
            <button
              className="ui yellow button"
              onClick={() => {
                this.onAddCart(e);
              }}
            >
              Add to Cart
            </button>
          </div>
        </div>
      );
    });
    return <div className=" grid-content">{img}</div>;
  }
}

export default SearchList;
