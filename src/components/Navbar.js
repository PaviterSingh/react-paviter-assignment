import React from "react";
import './NavBar.css';

class NavBar extends React.Component{
    state ={cat:''}
    // this function will call app component function
    onCategorySelect = (e)=>{
    this.props.onCategorySelect(this.state.cat);
    }
    render(){
        return(<div className="container"><ul>
            <li><button className="ui inverted yellow button" onClick={e =>this.props.onCategorySelect('Mobile')} value="Mobile">Mobile</button></li>
            <li><button className="ui inverted yellow button" onClick={e =>this.props.onCategorySelect('Laptop')}>Laptop</button></li>
            <li><button className="ui inverted yellow button" onClick={e =>this.props.onCategorySelect('Earphones')}>Earphones</button></li>
            <li className = "cart ui inverted yellow button" onClick={e =>this.props.onCategorySelect('Cart')} >Cart</li>
          </ul></div>);
    }
}

export default NavBar;