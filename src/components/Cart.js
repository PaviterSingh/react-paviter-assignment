import React from "react";
// logic of cart is most complex

var totalPrice = 0,
  itemsCount = 0;
class Cart extends React.Component {
    // return an array of items

    // function is 90% similar to SearchList 's logic
  creatItems = () => {
    const items = this.props.items.map((e) => {
        // for every item in cart calculating totalprice and itemscount
        // the ternary state is used when items quantity is null (note : the cart array does not have a attribute qyt)
      totalPrice += e.price * (e.qyt ? e.qyt : 1);
      itemsCount += Number(e.qyt ? e.qyt : 1);

      return (
        <div key={e.id} className="ui card grid-item">
          <div className="image">
            {/* <img  src = {require('../images/'+e.image) }/> */}
          </div>
          <div className="content">
            <p className="header">{e.name}</p>
            <div className="description">
              {e.description}
              <br />
              <h4>Price: {e.price} rupees</h4>
            </div>
          </div>
          <div className="extra content ">
            <h3 style={{ display: "inline" }}>Qty: {e.qyt ? e.qyt : 1} </h3>
            <input
              type="number"
              style={{ width: "17%", marginRight: "30px" }}
              onChange={(et) => {
                this.onQuantityChange(e, et.target.value);
              }}
              placeholder="1"
              min="1"
            />
            <button
              className="ui yellow button"
              onClick={() => this.onDeleteItem(e.id)}
            >
              Detete item
            </button>
          </div>
        </div>
      );
    });
    return items;
  };
  // called when delete button is clicked
  onDeleteItem = (id) => {
    if (id === "all") {
        // (note : intially i do't want to use alert, but did not find anything usefull)
      alert(
        `thanks for Buying stuff your order has been placed Items ${itemsCount} Total Amount ${totalPrice}`
      );
    }
    this.props.onDeleteItem(id);
  };
  // function is called when input type number is changed
  onQuantityChange = (item, q) => {
    item.qyt = q;
    this.forceUpdate();
  };
  render() {
      // for every render totallprice is 0 and count to
    totalPrice = 0;
    itemsCount = 0;
    // if CArd is empty
    if (this.props.items.length === 0) {
      return (
        <div>
          <h1
            className="ui yellow header"
            style={{ position: "absolute", top: "40vh", left: "40vw" }}
          >
            Cart is Empty, Choose item to add
          </h1>
        </div>
      );
    }
    const item = this.creatItems();
    return (
      <div>
        <div
          style={{ margin: "2px", position: "sticky", top: "0", zIndex: "1" }}
        >
            {/* this header replease the total count and amount */}
          <h2>
            Subtotal ({itemsCount} items):{" "}
            {/* represent totalPrice in money format */}
            {totalPrice.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")} Rupees{" "}
            <button
              className="ui yellow button"
              style={{ float: "right" }}
              onClick={() => {
                this.onDeleteItem("all");
              }}
            >
              Proceed to Buy
            </button>
          </h2>
        </div>

        <div className="ui horizontal divider">Total</div>
        <div className=" grid-content">{item}</div>
      </div>
    );
  }
}
export default Cart;
